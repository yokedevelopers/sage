<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
 * Custom file upload extensions
 */
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	$mimes['m4v'] = 'video/mp4'; 
	return $mimes;
}

add_filter('upload_mimes', __NAMESPACE__ . '\\cc_mime_types');

/**
 *  Remove admin bar
 */
add_filter('show_admin_bar', '__return_false');

// Custom Settings Pages
if( function_exists('acf_add_options_page') ) {
	// add parent
	$parent = acf_add_options_page(array(
		'page_title'  => 'Site General Settings',
		'menu_title'  => 'Site Settings',
		'redirect'    => false
	));
	
	// add sub page
	acf_add_options_sub_page(array(
		'page_title'  => 'Developer',
		'menu_title'  => 'Developer',
		'parent_slug'   => $parent['menu_slug'],
	));
}

/**
 * Custom Post Types
 */
function create_custom_post_types() {
	register_post_type( 'service',
		array(
			'labels'            => array(
				'name'               => __( 'Services' ),
				'singular_name'      => __( 'Service' ),
				'menu_name'          => _x( 'Services', 'admin menu' ),
				'name_admin_bar'     => _x( 'Service', 'add new on admin bar'),
				'add_new'            => _x( 'Add New', 'service' ),
				'add_new_item'       => __( 'Add New Service' ),
				'new_item'           => __( 'New Service' ),
				'edit_item'          => __( 'Edit Service' ),
				'view_item'          => __( 'View Service' ),
				'all_items'          => __( 'All Services' ),
				'search_items'       => __( 'Search Services' ),
				'parent_item_colon'  => __( 'Parent Services:' ),
				'not_found'          => __( 'No services found.' ),
				'not_found_in_trash' => __( 'No services found in Trash.' )
			),
			'public'            => true,
			'show_in_menu'      => true,
			'has_archive'       => true,
			'show_in_nav_menus' => true,
			'supports'          => array( 'title', 'editor', 'thumbnail' ),
			'rewrite'           => array(
				'slug'       => 'services',
				'with_front' => false
			)
		)
	);
}
add_action( 'init', __NAMESPACE__ . '\\create_custom_post_types' );

/**
* Custom Taxonomies
*/
function create_custom_taxonomies() {
	register_taxonomy('event_category', 
		array('event'), array(
			'hierarchical' => true,
			'labels' => array(
				'name'              => __('Categories', ''),
				'singular_name'     => _x('category', 'taxonomy singular name', ''),
				'search_items'      => __('Search Categories', ''),
				'all_items'         => __('All Categories', ''),
				'parent_item'       => __('Parent Category', ''),
				'parent_item_colon' => __('Parent Category:', ''),
				'edit_item'         => __('Edit Category', ''),
				'update_item'       => __('Update Category', ''),
				'add_new_item'      => __('Add New Category', ''),
				'new_item_name'     => __('New Category', ''),
			),
			'show_ui' => true,
			'rewrite' => array(
				'slug'       => 'category',
				'with_front' => false,
			),
		)
	);
}
add_action('init', __NAMESPACE__ . '\\create_custom_taxonomies');