<?php
add_filter('acf/settings/google_api_key', function () {
	return get_field('google_maps_api_key' ,'options');
});