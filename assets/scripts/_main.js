(function($) {
    $(document).ready(function() {
        yoke.event.onReady();
        yoke.event.listeners();
    });

    $(window).load(function() { 
        yoke.event.onLoad(); 
    });
})(jQuery); 