/**
 * example
 * @class
 * @namespace yoke
 */

(function($, exports) {
    var conf  = yoke.conf,
        util  = yoke.util,
        event = yoke.event,
        fx    = yoke.fx;

    /**
     * onWindowResize
     * @extends yoke.event.onWindowResize
     */
    event.onWindowResize = util.extend(event.onWindowResize, function(){
        console.log('The window has resized');
    });

    /**
     * aFunction
     */
    exports.aFunction = function(){
        return console.log('this function can be called by typing yoke.example.aFunction');
    };
})(jQuery, yoke.example = {});