/**
 * event
 * @module
 * @namespace yoke/event
 * @requires jquery.min.js
 * @requires plugins/underscore.min.js
 * @author Angus Dowling <angusdowling@live.com.au>
 */

(function ($, exports) {
	/**
	 * listeners
	 */
	exports.listeners = function () {
		$(window).on('resize.yoke', _.debounce(exports.onWindowResize, 300));
		$(window).on('scroll.yoke', _.throttle(exports.onWindowScroll, 10));
	};

	/**
	 * onReady
	 */
	exports.onReady = function () { };

	/**
	 * onLoad
	 */
	exports.onLoad = function () { };

	/**
	 * onWindowScroll
	 */
	exports.onWindowScroll = function () { };

	/**
	 * onWindowResize
	 */
	exports.onWindowResize = function () {
		yoke.conf.win.w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		yoke.conf.win.h = window.innerHeight || document.body.offsetHeight || document.documentElement.clientHeight || $(window).height();

		for(var key in yoke.conf.bpRange){
			if(yoke.util.currentBreakpoint(key)){
				if(yoke.conf._activeBp !== key){
					yoke.conf._activeBp = key;
					$(document).trigger('breakpoint:' + key);
				}
			}
		}
	};
})(jQuery, yoke.event = {});
